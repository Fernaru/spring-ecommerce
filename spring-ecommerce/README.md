# Spring-Ecommerce

The service picks up the following url of local http://localhost:8900/api/v1

The complete url to perform the tests is: http://localhost:8900/api/v1/ecommerce/create/campaign

The Test is finalized and to carry out the first tests in case you want to check the complete operation implementing API First I leave in the mock folder the environment of postman and the collection of postman in addition to the url of the mock in postman to perform tests before viewing the code.

# url mock postman 
https://1eba1391-ce53-417c-9336-66d48d92487b.mock.pstmn.io

# Environment and collection name
- Ecommerce.postman_environment.json
- electronic commerce.postman_collection

# Jacoco and failsafe

The project has integrated jacoco not only to see the code graph but also to validate the code coverage expressed in the code this with the help of failsafe that helps us identify what level of code coverage we are experiencing in our development in the pom.xml is implemented you can go to see it and give a better idea of ​​the configuration that was carried out.

# Swagger

You can also find it, we can find the documentation of the code in swagger along with a functional test that will allow you to make requests to the service once it is up.

The url for the swagger is the following: http://localhost:8900/swagger-ui/index.html?configUrl=/V1/api-docs/swagger-config

# Datebase

The database is an h2 and this is started with the import.sql file integrated in the project resources.

The url to enter the database is: http://localhost:8900/h2-console

It is parameterized for through jpa and hibernate so that the existing entities generate the database and the tables according to the entity in function provided in the java code. These are the configuration lines that were implemented in the properties.yml to achieve this setting

spring:
  datasource:
    driverClassName: org.h2.Driver
    password: ''
    url: jdbc:h2:mem:testdb
    username: sa
  h2:
    console:
      enabled: true
  jpa:
    database-platform: org.hibernate.dialect.H2Dialect
    
    
By default the create and drop is implemented but it can be changed to the way that best suits the development for example    


