package com.es.ecommerce.application;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.es.ecommerce.application.dto.ResponseDTO;
import com.es.ecommerce.infrastructure.util.MessagesUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class EcommerceAbstractController<T> {

	protected final EcommerceAbstractService<T> service;
	protected final ModelMapper mapper;
	protected final Class<T> targetServiceClass;

	public EcommerceAbstractController(EcommerceAbstractService<T> service, ModelMapper mapper, Class<T> clazz) {
		this.service = service;
		this.mapper = mapper;
		this.targetServiceClass = clazz;
	}

	protected <R> ResponseEntity<?> manageRequest(R data, BindingResult result) throws Exception {
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			List<String> error = result.getFieldErrors().stream().map(MessagesUtils::formatErrorMessage)
					.collect(Collectors.toList());
			response.put("errors", error);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		log.info("Init create, received object: {}", data);
		T input = mapper.map(data, targetServiceClass);
		log.info("Init create, received object: {}", input);
		ResponseDTO res = mapper.map(service.create(input), ResponseDTO.class);
		response.put(MessagesUtils.NOTIFICATION, res);
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}
}
