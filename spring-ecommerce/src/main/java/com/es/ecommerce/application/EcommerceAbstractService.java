package com.es.ecommerce.application;

import com.es.ecommerce.application.dto.ResponseDTO;

public abstract class EcommerceAbstractService<T> {
	
	public EcommerceAbstractService() {
		super();
	}

  abstract public ResponseDTO create(T requestData) throws Exception;

}
