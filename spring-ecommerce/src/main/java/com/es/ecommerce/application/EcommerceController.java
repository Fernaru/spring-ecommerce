package com.es.ecommerce.application;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.es.ecommerce.application.dto.PriceDTO;
import com.es.ecommerce.application.dto.ResponseDTO;
import com.es.ecommerce.infrastructure.exception.NotificationExDTO;
import com.es.ecommerce.infrastructure.util.Endpoints;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "Notification pricing", content = {
				@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseDTO.class)) }),
		@ApiResponse(responseCode = "400", description = "El servidor no va a procesar la peticion realizada", content = @Content),
		@ApiResponse(responseCode = "404", description = "El recurso de la peticion no se ha podido encontrar", content = {
				@Content(mediaType = "application/json", schema = @Schema(implementation = NotificationExDTO.class)) }),
		@ApiResponse(responseCode = "500", description = "server error", content = {
				@Content(mediaType = "application/json", schema = @Schema(implementation = NotificationExDTO.class)) }) })

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(path = Endpoints.ECOMMERCE)
public class EcommerceController extends EcommerceAbstractController<PriceDTO> {

	public EcommerceController(@Qualifier("EcommerceServiceImpl") EcommerceAbstractService<PriceDTO> service,
			ModelMapper mapper) {
		super(service, mapper, PriceDTO.class);
	}

	@Operation(summary = "Create campaign for new price")
	@PostMapping(path = Endpoints.CREATEECOMMERCE)
	public ResponseEntity<?> create(@Valid @RequestBody PriceDTO price, BindingResult bindigResult) throws Exception {
		return super.manageRequest(price, bindigResult);
	}
}
