package com.es.ecommerce.application;

import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.es.ecommerce.application.dto.PriceDTO;
import com.es.ecommerce.application.dto.ResponseDTO;
import com.es.ecommerce.infrastructure.memory.EcommerceRepository;
import com.es.ecommerce.model.Price;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("EcommerceServiceImpl")
public class EcommerceServiceImpl extends EcommerceAbstractService<PriceDTO> {

	private EcommerceRepository repository;
	private ModelMapper mapper;

	public EcommerceServiceImpl(EcommerceRepository repository, ModelMapper mapper) {
		this.repository = repository;
		this.mapper = mapper;
	}

	@Override
	@Transactional
	public ResponseDTO create(PriceDTO data) throws Exception {
		Price price = new Price();
		price.setPriority(validPriority(data));
		price = mapper.map(data, Price.class);
		return mapper.map(repository.save(price), ResponseDTO.class);
	}

	private Integer validPriority(PriceDTO data) {
		List<Price> list = repository.findAll();
		if (list.stream()
				.filter(e -> e.getDateStar().isEqual(data.getDateStar()) && e.getDateEnd().isEqual(data.getDateEnd())) != null) {
			return 2;
		} else {
			return 1;
		}
		
	}

}
