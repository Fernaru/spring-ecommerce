package com.es.ecommerce.application.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PriceDTO {

	@NotNull
	@Schema(example = "1", description = "id externo para identificar de donde proviene el producto", required = true)
	private Long idBrand;
	@NotNull
	@Schema(example = "2021-11-14T10:00:00", description = "fecha de inicio de la campaña", required = true)
	private LocalDateTime dateStar;
	@NotNull
	@Schema(example = "2021-12-31T16:00:00", description = "fecha fin de la campaña", required = true)
	private LocalDateTime dateEnd;
	@NotNull
	@Schema(example = "35455", description = "id del producto en campaña", required = true)
	private Long idProduct;

}
