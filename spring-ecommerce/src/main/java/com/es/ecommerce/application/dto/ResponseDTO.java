package com.es.ecommerce.application.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDTO {

	private Long idProduct;
	private Long idBrand;
	private Integer priceList;
	private LocalDateTime dateStar;
	private LocalDateTime dateEnd;
	private Double price;
}
