package com.es.ecommerce.infrastructure.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {
  /**
   * Swagger config
   *
   * @return OpenAPI OpenAPI
   */
  @Bean
  public OpenAPI openApi() {
    Contact contact = new Contact().name("Ecommerce");

    OpenAPI openApi = new OpenAPI()
        .info(new Info().title("Ecommerce-service").contact(contact).version("V1"));

    return openApi;
  }


}
