package com.es.ecommerce.infrastructure.exception;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import com.es.ecommerce.infrastructure.util.ErrorCodes;

@SuppressWarnings("serial")
public class BusinessException extends EcommerceRestException {

	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	public BusinessException(String message) {
		super(message);
	}

	public BusinessException(MessageSource messageSource) {
		super(messageSource.getMessage(ErrorCodes.E501, null, LocaleContextHolder.getLocale()), ErrorCodes.E501);
	}
}
