package com.es.ecommerce.infrastructure.exception;


public class EcommerceRestException extends RuntimeException {

  private static final long serialVersionUID = 4766837649724934235L;
  private String errorCode;

  public EcommerceRestException(String message, Throwable cause) {
    super(message, cause);
  }

  public EcommerceRestException(String message) {
    super(message);
  }

  public EcommerceRestException(String message, String errorCode) {
    super(message);
    this.errorCode = errorCode;
  }

  /**
   * @return the errorCode
   */
  public String getErrorCode() {
    return errorCode;
  }

  /**
   * @param errorCode the errorCode to set
   */
  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }
}
