package com.es.ecommerce.infrastructure.exception;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NotificationExDTO {

  private Integer status;
  private String errorDetail;
  private List<String> errors;
  private Object generic;
}
