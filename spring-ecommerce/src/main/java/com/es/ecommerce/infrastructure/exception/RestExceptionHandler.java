package com.es.ecommerce.infrastructure.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestExceptionHandler {

	@ExceptionHandler({ BusinessException.class })
	public ResponseEntity<?> handleEntityNotFound(BusinessException ex) {
		return new ResponseEntity<>(NotificationExDTO.builder().status(1).errorDetail(ex.getMessage()).build(), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler({ TechnicalException.class })
	public ResponseEntity<?> handleEntityInternelServerError(TechnicalException ex) {
		return new ResponseEntity<>(NotificationExDTO.builder().status(1).errorDetail(ex.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
