package com.es.ecommerce.infrastructure.exception;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import com.es.ecommerce.infrastructure.util.ErrorCodes;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TechnicalException extends EcommerceRestException {

	private static final long serialVersionUID = 1L;

	public TechnicalException(MessageSource messageSource) {
		super(messageSource.getMessage(ErrorCodes.E502, null, LocaleContextHolder.getLocale()), ErrorCodes.E502);
	}

	public TechnicalException(String string) {
		super(string, ErrorCodes.E503);
	}
}
