package com.es.ecommerce.infrastructure.memory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.es.ecommerce.model.Price;

@Repository
public interface EcommerceRepository extends JpaRepository<Price, Long> {

}
