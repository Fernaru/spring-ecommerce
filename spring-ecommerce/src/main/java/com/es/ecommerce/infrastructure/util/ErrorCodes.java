package com.es.ecommerce.infrastructure.util;

import org.springframework.beans.factory.annotation.Value;

public class ErrorCodes {

	@Value("errorcode.code.E501")
	public static final String E501 = "501";
	@Value("errorcode.code.E502")
	public static final String E502 = "502";
	@Value("errorcode.code.E503")
	public static final String E503 = "503";
	@Value("errorcode.code.E504")
	public static final String E504 = "504";
	
}
