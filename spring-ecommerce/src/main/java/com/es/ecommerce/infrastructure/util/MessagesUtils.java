package com.es.ecommerce.infrastructure.util;

import org.springframework.validation.FieldError;

public class MessagesUtils {

	public static final String NOTIFICATION = "Notificacion";
	public static final String OKCREATE = "El envio de los eventos finalizo correctamente";
	public static final String INIT = "Inicio creacion";
	public static final String END = "Fin de creacion";
	public static final Integer VALID = 1;

	public static final String formatErrorMessage(FieldError err) {
		return new StringBuilder().append("El campo '").append(err.getField()).append("' el mensaje ")
				.append(err.getDefaultMessage()).toString();
	}

}
