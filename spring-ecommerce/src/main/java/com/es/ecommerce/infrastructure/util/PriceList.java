package com.es.ecommerce.infrastructure.util;

public enum PriceList {

	TARIFA_1("tarifa 1", 1), TARIFA_2("tarifa 2", 2), TARIFA_3("tarifa 3", 3), TARIFA_4("tarifa 4", 4);
	
	
	private String tarifa;
	private Integer idTarifa;
	
	
	private PriceList(String tarifa, Integer idTarifa) {
		this.tarifa = tarifa;
		this.idTarifa = idTarifa;
	}


	public String getTarifa() {
		return tarifa;
	}

	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}

	public Integer getIdTarifa() {
		return idTarifa;
	}

	public void setIdTarifa(Integer idTarifa) {
		this.idTarifa = idTarifa;
	}
	
	
}
