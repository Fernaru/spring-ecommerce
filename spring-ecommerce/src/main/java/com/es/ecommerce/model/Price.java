package com.es.ecommerce.model;

import java.time.LocalDateTime;
import java.util.concurrent.ThreadLocalRandom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.es.ecommerce.infrastructure.util.MessagesUtils;
import com.es.ecommerce.infrastructure.util.PriceList;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "prices")
public class Price {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "brand_id")
	private Long idBrand;
	@Column(name = "start_date")
	private LocalDateTime dateStar;
	@Column(name = "end_date")
	private LocalDateTime dateEnd;
	@Column(name = "price_list")
	private Integer priceList;
	@Column(name = "product_id")
	private Long idProduct;
	@Column(name = "priority")
	private Integer priority;
	@Column(name = "price")
	private Double price;
	@Column(name = "curr")
	private String curr;
	@Column(name = "last_update")
	private LocalDateTime lastUpdate;
	@Column(name = "last_update_by")
	private String lastUpdateBy;

	@PrePersist
	public void prePersist() {
		this.lastUpdate = LocalDateTime.now();
		this.lastUpdateBy = "user1";
		this.price = ThreadLocalRandom.current().nextDouble(10.01, 99.99);
		this.curr = "EUR";
		this.priceList= MessagesUtils.VALID;
	}

	@PreUpdate
	public void preUpdate() {
		this.lastUpdate = LocalDateTime.now();
		this.lastUpdateBy = "user1";
	}

	public void getPriceList(Integer valid) {
		switch (valid) {
		case 1:
			this.priceList = PriceList.TARIFA_1.getIdTarifa();
			break;
		case 2:
			this.priceList = PriceList.TARIFA_2.getIdTarifa();
			break;
		case 3:
			this.priceList = PriceList.TARIFA_3.getIdTarifa();
			break;
		case 4:
			this.priceList = PriceList.TARIFA_4.getIdTarifa();
			break;
		default:
			this.priceList = PriceList.TARIFA_1.getIdTarifa();
			break;
		}
	}

	public boolean isIdBrandEmpty() {
		return (this.idBrand == null)?false:true;
	}

	public boolean isIdProductEmpty() {
		return (this.idProduct == null)?false:true;
	}
	
}
