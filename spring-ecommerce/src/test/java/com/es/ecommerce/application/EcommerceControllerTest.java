package com.es.ecommerce.application;

import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.es.ecommerce.application.dto.PriceDTO;
import com.es.ecommerce.application.dto.ResponseDTO;
import com.es.ecommerce.factory.EcommerceFactory;
import com.es.ecommerce.model.Price;

class EcommerceControllerTest {

	@Qualifier("EcommerceServiceImpl")
	private EcommerceAbstractService<PriceDTO> service;
	private EcommerceController controller;
	private ModelMapper modelMapper;

	@SuppressWarnings("unchecked")
	@BeforeEach
	public void setUp() {
		modelMapper = mock(ModelMapper.class);
		service = mock(EcommerceAbstractService.class);
		controller = new EcommerceController(service, modelMapper);
	}

	@Test
	void shouldResponseRequestOkCode201() throws Exception {
		BindingResult result = mock(BindingResult.class);
		Mockito.when(result.hasErrors()).thenReturn(false);
		Mockito.when(modelMapper.map(EcommerceFactory.inputDto(), Price.class)).thenReturn(EcommerceFactory.create());
		Mockito.when(modelMapper.map(EcommerceFactory.create(), ResponseDTO.class))
				.thenReturn(EcommerceFactory.responseDTO());
		Mockito.when(service.create(EcommerceFactory.inputDto())).thenReturn(EcommerceFactory.responseDTO());
		ResponseEntity<?> response = controller.create(EcommerceFactory.inputDto(), result);
		Assertions.assertEquals(response.getStatusCode(), HttpStatus.CREATED);
	}

	@Test
	void shouldResponseRequestFailedCode500() throws Exception {
		BindingResult result = mock(BindingResult.class);
		Mockito.when(result.hasErrors()).thenReturn(true);
		Mockito.when(modelMapper.map(EcommerceFactory.inputDto(), Price.class)).thenReturn(EcommerceFactory.create());
		Mockito.when(modelMapper.map(EcommerceFactory.create(), ResponseDTO.class))
				.thenReturn(EcommerceFactory.responseDTO());
		Mockito.when(service.create(EcommerceFactory.inputDto())).thenReturn(EcommerceFactory.responseDTO());
		ResponseEntity<?> response = controller.create(EcommerceFactory.inputDto(), result);
		Assertions.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
	}
}
