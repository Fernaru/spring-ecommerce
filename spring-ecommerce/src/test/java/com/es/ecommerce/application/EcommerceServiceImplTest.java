package com.es.ecommerce.application;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;

import com.es.ecommerce.application.dto.ResponseDTO;
import com.es.ecommerce.factory.EcommerceFactory;
import com.es.ecommerce.infrastructure.memory.EcommerceRepository;
import com.es.ecommerce.model.Price;

class EcommerceServiceImplTest {

	private EcommerceRepository repository;
	private EcommerceServiceImpl impl;
	private ModelMapper modelMapper;
	private Price price;

	@BeforeEach
	public void setUp() {
		modelMapper = mock(ModelMapper.class);
		repository = mock(EcommerceRepository.class);
		price = mock(Price.class);
		impl = new EcommerceServiceImpl(repository, modelMapper);
	}

	@Test
	void shouldResponseRequestOkCode() throws Exception {
		doNothing().when(price).setPriceList(1);
		Mockito.when(repository.findAll()).thenReturn(EcommerceFactory.createList());
		Mockito.when(repository.save(EcommerceFactory.createImpl())).thenReturn(EcommerceFactory.create());
		Mockito.when(modelMapper.map(EcommerceFactory.create(), ResponseDTO.class))
				.thenReturn(EcommerceFactory.responseDTO());
		impl.create(EcommerceFactory.inputDto());
	}

	@Test
	void shouldResponseRequestOkCodeOtherTest() throws Exception {
		doNothing().when(price).setPriceList(2);
		Mockito.when(repository.findAll()).thenReturn(EcommerceFactory.createList());
		Mockito.when(repository.save(EcommerceFactory.createImpl())).thenReturn(EcommerceFactory.create());
		Mockito.when(modelMapper.map(EcommerceFactory.create(), ResponseDTO.class))
				.thenReturn(EcommerceFactory.responseDTO());
		impl.create(EcommerceFactory.inputDto());
	}
	
}
