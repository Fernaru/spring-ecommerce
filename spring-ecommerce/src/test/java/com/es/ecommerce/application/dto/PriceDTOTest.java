package com.es.ecommerce.application.dto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.es.ecommerce.application.dto.PriceDTO.PriceDTOBuilder;

class PriceDTOTest {

	PriceDTO WithData;
	PriceDTO NoData;

	@BeforeEach
	void init() {
		PriceDTOBuilder builder = PriceDTO.builder();
		builder.idBrand(Long.valueOf(1));
		builder.idProduct(Long.valueOf(1));
		builder.dateEnd(null);
		builder.dateStar(null);
		WithData = builder.build();
		NoData = PriceDTO.builder().build();
	}

	@Test
	void shouldwithrequesstdata() {
		Assertions.assertEquals(WithData.getIdBrand(), Long.valueOf(1));
		Assertions.assertEquals(WithData.getIdProduct(), Long.valueOf(1));
		Assertions.assertEquals(WithData.getDateEnd(), null);
		Assertions.assertEquals(WithData.getDateStar(), null);
		Assertions.assertTrue(WithData.equals(WithData) && NoData.equals(NoData));
		Assertions.assertTrue(WithData.hashCode() == WithData.hashCode());
		String expected = WithData.toString();
		Assertions.assertEquals(expected, WithData.toString());
	}
	
	@Test
	void shouldwithrequesstdataTostring() {
		String expected = WithData.toString();
		Assertions.assertEquals(expected, WithData.toString());
	}

}
