package com.es.ecommerce.application.dto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.es.ecommerce.application.dto.ResponseDTO.ResponseDTOBuilder;

class ResponseDTOTest {

	ResponseDTO WithData;
	ResponseDTO NoData;

	@BeforeEach
	void init() {
		ResponseDTOBuilder builder = ResponseDTO.builder();
		builder.idBrand(Long.valueOf(1));
		builder.idProduct(Long.valueOf(1));
		builder.dateEnd(null);
		builder.dateStar(null);
		WithData = builder.build();
		NoData = ResponseDTO.builder().build();
	}

	@Test
	void shouldwithrequesstdata() {
		Assertions.assertEquals(WithData.getIdBrand(), Long.valueOf(1));
		Assertions.assertEquals(WithData.getIdProduct(), Long.valueOf(1));
		Assertions.assertEquals(WithData.getDateEnd(), null);
		Assertions.assertEquals(WithData.getDateStar(), null);
		Assertions.assertTrue(WithData.equals(WithData) && NoData.equals(NoData));
		Assertions.assertTrue(WithData.hashCode() == WithData.hashCode());
	}

	@Test
	void shouldwithrequesstdataTostring() {
		String expected = WithData.toString();
		Assertions.assertEquals(expected, WithData.toString());
	}
}
