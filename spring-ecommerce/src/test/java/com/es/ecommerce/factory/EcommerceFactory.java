package com.es.ecommerce.factory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.es.ecommerce.application.dto.PriceDTO;
import com.es.ecommerce.application.dto.ResponseDTO;
import com.es.ecommerce.model.Price;

public class EcommerceFactory {

	public static final Date START_DATE = new Date();
	public static final Date END_DATE = new Date();
	public static final String CURR = "EUC";
	public static final String UPDATEBY = "user1";

	public static String getValidWebRequest(String jsonName) throws Exception {
		URL resource = EcommerceFactory.class.getClassLoader().getResource(jsonName);
		return FileUtils.readFileToString(new File(resource.getFile()));
	}

	public static Price create() throws IOException {
		Price price = new Price();
		price.setId(Long.valueOf(1));
		price.setIdBrand(Long.valueOf(1));
		price.setCurr(CURR);
		price.setDateEnd(LocalDateTime.now());
		price.setDateStar(LocalDateTime.now());
		price.setId(Long.valueOf(1));
		price.setIdProduct(Long.valueOf(1));
		price.setPriceList(1);
		price.setPriority(1);
		price.setPrice(Double.parseDouble("35.12"));
		price.setLastUpdate(LocalDateTime.now());
		price.setLastUpdateBy(UPDATEBY);
		return price;
	}
	
	public static List<Price> createList() throws IOException {
		List<Price> list = new ArrayList<>();
		Price price = new Price();
		price.setId(Long.valueOf(1));
		price.setIdBrand(Long.valueOf(1));
		price.setCurr(CURR);
		price.setDateEnd(LocalDateTime.now());
		price.setDateStar(LocalDateTime.now());
		price.setId(Long.valueOf(1));
		price.setIdProduct(Long.valueOf(1));
		price.setPriceList(1);
		price.setPriority(1);
		price.setPrice(Double.parseDouble("35.12"));
		price.setLastUpdate(LocalDateTime.now());
		price.setLastUpdateBy(UPDATEBY);
		list.add(price);
		return list;
	}

	public static PriceDTO inputDto() {
		PriceDTO dto = new PriceDTO();
		dto.setDateEnd(LocalDateTime.now());
		dto.setDateStar(LocalDateTime.now());
		dto.setIdBrand(Long.valueOf(1));
		dto.setIdProduct(Long.valueOf(1));
		return dto;
	}

	public static ResponseDTO responseDTO() {
		ResponseDTO resDto = new ResponseDTO();
		resDto.setDateEnd(LocalDateTime.now());
		resDto.setDateStar(LocalDateTime.now());
		resDto.setIdBrand(Long.valueOf(1));
		resDto.setIdProduct(Long.valueOf(1));
		resDto.setPrice(Double.parseDouble("35.12"));
		resDto.setPriceList(1);
		return resDto;
	}

	public static Price createImpl() {
		Price price = new Price();
		price.setId(Long.valueOf(1));
		price.setIdBrand(Long.valueOf(1));
		price.setDateEnd(LocalDateTime.now());
		price.setDateStar(LocalDateTime.now());
		price.setId(Long.valueOf(1));
		price.setIdProduct(Long.valueOf(1));
		price.setPriceList(1);
		price.setPriority(1);
		return price;
	}

}
