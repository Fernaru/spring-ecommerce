package com.es.ecommerce.model;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.es.ecommerce.model.Price.PriceBuilder;

class PriceTest {

	Price WithData;
	Price NoData;

	
	@BeforeEach
	void init() {
		PriceBuilder builder = Price.builder();
		builder.id(Long.valueOf(1));
		builder.idBrand(Long.valueOf(1));
		builder.idProduct(Long.valueOf(1));
		builder.curr("a1");
		builder.dateEnd(null);
		builder.dateStar(null);
		builder.lastUpdate(null);
		builder.lastUpdateBy("user1");
		builder.price(Double.parseDouble("35.40"));
		builder.priority(1);
		builder.priceList(1);
		WithData = builder.build();
		NoData = Price.builder().build();
	}
	
	@Test
	void isEmptyTest() {
		assertAll(
				() -> assertFalse(NoData.isIdBrandEmpty()),
				() -> assertTrue(WithData.isIdBrandEmpty()),
				
				() -> assertFalse(NoData.isIdProductEmpty()),
				() -> assertTrue(WithData.isIdProductEmpty())
		);
	}
	
	@Test
	void shouldwithrequesstdata() {
		Assertions.assertEquals(WithData.getPrice(), Double.parseDouble("35.40"));
		Assertions.assertEquals(WithData.getId(),Long.valueOf(1));
		Assertions.assertEquals(WithData.getIdBrand(), Long.valueOf(1));
		Assertions.assertEquals(WithData.getIdProduct(), Long.valueOf(1));
		Assertions.assertEquals(WithData.getCurr(), "a1");
		Assertions.assertEquals(WithData.getPriority(), 1);
		Assertions.assertEquals(WithData.getPriceList(), 1);
		Assertions.assertEquals(WithData.getDateEnd(), null);
		Assertions.assertEquals(WithData.getDateStar(), null);
		Assertions.assertEquals(WithData.getLastUpdate(), null);
		Assertions.assertEquals(WithData.getLastUpdateBy(), "user1");
		Assertions.assertTrue(WithData.equals(WithData) && NoData.equals(NoData));
		Assertions.assertTrue(WithData.hashCode() == WithData.hashCode());
	}
	
	@Test
	void shouldwithrequesstdataSwicht() {
		WithData.getPriceList(1);
		WithData.getPriceList(2);
		WithData.getPriceList(3);
		WithData.getPriceList(4);
		WithData.getPriceList(5);
		WithData.prePersist();
		WithData.preUpdate();
	}
	
	@Test
	void shouldwithrequesstdataTostring() {
		String expected = WithData.toString();
		Assertions.assertEquals(expected, WithData.toString());
	}
}
